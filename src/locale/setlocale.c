/*
 * setlocale: change or query the current locale.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <locale.h>
#include <string.h>
#include <cstd/locale.h>

extern struct __cstd_locale __c_conv;
struct __cstd_locale *__cstd_conv = &__c_conv;

char *(setlocale)(int category, const char *locale)
{
	if (locale == NULL)
		return "C";
	if (strcmp(locale, "") == 0)
		return "C";
	if (strcmp(locale, "C") == 0)
		return "C";
	return NULL;
}
