/*
 * Wide character conversion for the "C" locale.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <limits.h>
#include <cstd/locale.h>

static wint_t
c_decode(const char * restrict s, char ** restrict end, size_t n)
{
	wint_t rc = WEOF;

	if (n > 0) {
		n  = 1;
		rc = *(unsigned char *)s;
	}

	if (end != NULL)
		*end = (char *)s+n;
	return rc;
}

static size_t
c_encode(char *s, wint_t c)
{
	if (c < 0 || c > UCHAR_MAX)
		return 0;

	*(unsigned char *)s = c;
	return 1;
}

struct __cstd_convfuncs __c_conv = {
	.maxlen = 1,
	.decode = c_decode,
	.encode = c_encode,
};
