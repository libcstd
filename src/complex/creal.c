/*
 * creal: Extract the real component from a complex number.
 * Copyright (C) 2010 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#include <complex.h>
#include <cstd/mathcommon.h>

TYPE MF(creal)(TYPE complex x)
{
	TYPE (*a)[2] = (TYPE(*)[2])&x;
	return (*a)[0];
}
