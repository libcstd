/*
 * conj: Complex conjugate function.
 * Copyright (C) 2010 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#include <complex.h>
#include <cstd/mathcommon.h>

TYPE complex MF(conj)(TYPE complex x)
{
	TYPE (*a)[2] = (TYPE(*)[2])&x;

	if (signbit((*a)[1]))
		(*a)[1] = MF(copysign)((*a)[1],  1.0);
	else
		(*a)[1] = MF(copysign)((*a)[1], -1.0);

	return x;
}
