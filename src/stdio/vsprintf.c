/*
 * Formatted input/output support.
 * Copyright (C) 2010 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdarg.h>

int (vsprintf)(char *str, const char *fmt, va_list ap)
{
	return vsnprintf(str, -1, fmt, ap);
}
