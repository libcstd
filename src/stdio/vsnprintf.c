/*
 * Formatted input/output support.
 * Copyright (C) 2010 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <inttypes.h>
#include <limits.h>
#include <stdarg.h>
#include <assert.h>
#include <ctype.h>
#include <wchar.h>

/* ptrdiff_t can be promoted to int or be unchanged. */
#if PTRDIFF_MIN >= INT_MIN && PTRDIFF_MAX <= INT_MAX
typedef int ptrdiff_p;
#else
typedef ptrdiff_t ptrdiff_p;
#endif

/* size_t can be promoted to int, unsigned or be unchanged. */
#if SIZE_MAX <= INT_MAX
typedef int size_p;
#elif SIZE_MAX <= UINT_MAX
typedef unsigned size_p;
#else
typedef size_t size_p;
#endif

/* unsigned char can be promoted to int or unsigned. */
#if UCHAR_MAX <= INT_MAX
typedef int uchar_p;
#else
typedef unsigned uchar_p;
#endif

/* unsigned short can be promoted to one of int or unsigned. */
#if USHRT_MAX <= INT_MAX
typedef int ushrt_p;
#else
typedef unsigned ushrt_p;
#endif

enum {
	LENGTH_DEFAULT,
	LENGTH_CHAR,
	LENGTH_SHRT,
	LENGTH_LONG,
	LENGTH_LLONG,
	LENGTH_INTMAX,
	LENGTH_PTRDIFF,
	LENGTH_SIZE,
	LENGTH_LDBL
};

enum {
	CONV_SIGNED,
	CONV_UNSIGNEDO,
	CONV_UNSIGNEDU,
	CONV_UNSIGNEDX,
	CONV_FLOATF,
	CONV_FLOATE,
	CONV_FLOATG,
	CONV_FLOATA,
	CONV_CHAR,
	CONV_STRING,
	CONV_POINTER,
	CONV_OUTPUT,
	CONV_PERCENT,
};

struct conv {
	int specifier, length, width, precision;

	union {
		long double floating;
		intmax_t    signedint;
		uintmax_t   unsignedint;
		wint_t      wideint;

		void *pointer;
	} val;

	_Bool left_justify :1,
	      always_sign  :1,
	      pad_sign     :1,
	      alt_form     :1,
	      lead_zeros   :1,
	      capitalise   :1;
};

/*
 * Parse a conversion specifier, not including the initial '%' character.
 * Variadic arguments are consumed according to the specifier.  The given conv
 * structure is filled out accordingly, and the number of characters in the
 * specifier is returned.
 *
 * If the conversion specifier is invalid, the behaviour is undefined.
 */
static size_t parse_conv(const char *s, struct conv *conv, va_list ap)
{
	unsigned long val;
	long val2;
	char *endptr;
	int state = 0;
	size_t i;

	*conv = (struct conv){.precision = -1};

	for (i = 0; state < 5 && s[i]; i++) {
		switch (state) {
		case 0: /* flags */
			switch (s[i]) {
			case '-': conv->left_justify = 1; continue;
			case '+': conv->always_sign  = 1; continue;
			case ' ': conv->pad_sign     = 1; continue;
			case '#': conv->alt_form     = 1; continue;
			case '0': conv->lead_zeros   = 1; continue;
			}

			state = 1; /* fall through */
		case 1: /* field width */
			val = strtoul(&s[i], &endptr, 10);
			if (val > 0) {
				conv->width = (val <= INT_MAX ? val : INT_MAX);
				i += endptr - &s[i];
			} else if (s[i] == '*') {
				conv->width = va_arg(ap, int);
				i++;
			}

			if (conv->width < 0) {
				conv->left_justify = 1;
				conv->width = conv->width == INT_MIN
					? INT_MAX : abs(conv->width);
			}

			if (s[i] == '.')
				state = 2;
			else
				goto state_3;
			continue;
		case 2: /* precision */
			if (s[i] == '*') {
				conv->precision = va_arg(ap, int);
				i++;
			} else {
				val2 = strtol(&s[i], &endptr, 10);
				if (val2 > INT_MAX) val2 = INT_MAX;
				if (val2 < INT_MIN) val2 = INT_MIN;
				conv->precision = val2;
				i += endptr - &s[i];
			}
			state = 3; /* fall through */
		case 3: /* length modifier */
		state_3:
			switch (s[i]) {
			case 'l':
				if (!conv->length) {
					conv->length = LENGTH_LONG;
					continue;
				}
				conv->length = LENGTH_LLONG;
				continue;
			case 'h':
				if (!conv->length) {
					conv->length = LENGTH_SHRT;
					continue;
				}
				conv->length = LENGTH_CHAR;
				continue;
			case 'j': conv->length = LENGTH_INTMAX;  continue;
			case 'z': conv->length = LENGTH_SIZE;    continue;
			case 't': conv->length = LENGTH_PTRDIFF; continue;
			case 'L': conv->length = LENGTH_LDBL;    continue;
			}

			state = 4; /* fall through */
		case 4:
			switch (s[i]) {
			case 'd': conv->specifier = CONV_SIGNED;    break;
			case 'i': conv->specifier = CONV_SIGNED;    break;
			case 'o': conv->specifier = CONV_UNSIGNEDO; break;
			case 'u': conv->specifier = CONV_UNSIGNEDU; break;
			case 'X': conv->capitalise = 1; /* fall through */
			case 'x': conv->specifier = CONV_UNSIGNEDX; break;
			case 'F': conv->capitalise = 1; /* fall through */
			case 'f': conv->specifier = CONV_FLOATF;    break;
			case 'E': conv->capitalise = 1; /* fall through */
			case 'e': conv->specifier = CONV_FLOATE;    break;
			case 'G': conv->capitalise = 1; /* fall through */
			case 'g': conv->specifier = CONV_FLOATG;    break;
			case 'A': conv->capitalise = 1; /* fall through */
			case 'a': conv->specifier = CONV_FLOATA;    break;
			case 'c': conv->specifier = CONV_CHAR;      break;
			case 's': conv->specifier = CONV_STRING;    break;
			case 'p': conv->specifier = CONV_POINTER;   break;
			case 'n': conv->specifier = CONV_OUTPUT;    break;
			case '%': conv->specifier = CONV_PERCENT;   break;
			default:
				assert(!"invalid conversion specifier");
			}

			state = 5;
			continue;
		}
	}

	/* Now simply obtain the value from the va_list. */
	switch (conv->specifier) {
	case CONV_SIGNED:
		switch (conv->length) {
		case LENGTH_CHAR:
			conv->val.signedint = (signed char)va_arg(ap, int);
			break;
		case LENGTH_SHRT:
			conv->val.signedint = (short)va_arg(ap, int);
			break;
		case LENGTH_DEFAULT:
			conv->val.signedint = va_arg(ap, int);
			break;
		case LENGTH_LONG:
			conv->val.signedint = va_arg(ap, long);
			break;
		case LENGTH_LLONG:
			conv->val.signedint = va_arg(ap, long long);
			break;
		case LENGTH_PTRDIFF:
			conv->val.signedint = va_arg(ap, ptrdiff_p);
			break;
		case LENGTH_SIZE:
			assert(!"%zd is not supported yet...");
			break;
		case LENGTH_INTMAX:
			conv->val.signedint = va_arg(ap, intmax_t);
			break;
		default:
			assert(!"invalid length for signed conversion");
		}
		break;
	case CONV_UNSIGNEDO:
	case CONV_UNSIGNEDU:
	case CONV_UNSIGNEDX:
		switch (conv->length) {
		case LENGTH_CHAR:
			conv->val.unsignedint = (unsigned char)va_arg(ap, uchar_p);
			break;
		case LENGTH_SHRT:
			conv->val.unsignedint = (unsigned short)va_arg(ap, ushrt_p);
			break;
		case LENGTH_DEFAULT:
			conv->val.unsignedint = va_arg(ap, unsigned);
			break;
		case LENGTH_LONG:
			conv->val.unsignedint = va_arg(ap, unsigned long);
			break;
		case LENGTH_LLONG:
			conv->val.unsignedint = va_arg(ap, unsigned long long);
			break;
		case LENGTH_PTRDIFF:
			assert(!"%tu is not supported yet...");
			break;
		case LENGTH_SIZE:
			conv->val.unsignedint = va_arg(ap, size_p);
			break;
		case LENGTH_INTMAX:
			conv->val.unsignedint = va_arg(ap, uintmax_t);
			break;
		default:
			assert(!"invalid length for unsigned conversion");
		}
		break;
	case CONV_FLOATF:
	case CONV_FLOATE:
	case CONV_FLOATG:
	case CONV_FLOATA:
		switch (conv->length) {
		case LENGTH_LONG:
		case LENGTH_DEFAULT:
			conv->val.floating = va_arg(ap, double);
			break;
		case LENGTH_LDBL:
			conv->val.floating = va_arg(ap, long double);
			break;
		default:
			assert(!"invalid length for floating conversion");
		}
	case CONV_CHAR:
		switch (conv->length) {
		case LENGTH_DEFAULT:
			conv->val.unsignedint = (unsigned char)va_arg(ap, int);
			break;
		case LENGTH_LONG:
			conv->val.wideint = va_arg(ap, wint_t);
			break;
		default:
			assert(!"invalid length for character conversion");
		}
		break;
	case CONV_STRING:
		switch (conv->length) {
		case LENGTH_DEFAULT:
			conv->val.pointer = va_arg(ap, char *);
			break;
		case LENGTH_LONG:
			conv->val.pointer = va_arg(ap, wchar_t *);
			break;
		default:
			assert(!"invalid length for string conversion");
		}
		break;
	case CONV_POINTER:
		conv->val.pointer = va_arg(ap, void *);
		break;
	case CONV_OUTPUT:
		switch (conv->length){
		case LENGTH_DEFAULT:
			conv->val.pointer = va_arg(ap, int *);
			break;
		case LENGTH_CHAR:
			conv->val.pointer = va_arg(ap, signed char *);
			break;
		case LENGTH_SHRT:
			conv->val.pointer = va_arg(ap, short *);
			break;
		case LENGTH_LONG:
			conv->val.pointer = va_arg(ap, long *);
			break;
		case LENGTH_LLONG:
			conv->val.pointer = va_arg(ap, long long *);
			break;
		case LENGTH_PTRDIFF:
			conv->val.pointer = va_arg(ap, ptrdiff_t *);
			break;
		case LENGTH_SIZE:
			assert(!"%zn is not supported yet...");
			break;
		case LENGTH_INTMAX:
			conv->val.pointer = va_arg(ap, intmax_t *);
			break;
		default:
			assert(!"invalid length for output conversion");
		}
		break;
	}

	/* Final fixups of the result. */
	if (conv->left_justify || conv->precision >= 0)
		conv->lead_zeros = 0;

	if (conv->precision < 0) {
		switch (conv->specifier) {
		case CONV_SIGNED:
		case CONV_UNSIGNEDU:
		case CONV_UNSIGNEDO:
		case CONV_UNSIGNEDX:
			conv->precision = 1;
			break;
		default:
			conv->precision = 0;
		}
	}

	return i;
}

static char from_base(int v)
{
	switch (v) {
		case 10: return 'a';
		case 11: return 'b';
		case 12: return 'c';
		case 13: return 'd';
		case 14: return 'e';
		case 15: return 'f';
	}

	return '0' + v;
}

static size_t
render_unsigned(char *buf, size_t size, size_t skip, int base, struct conv *c)
{
	uintmax_t tmp = c->val.unsignedint;
	size_t width;

	for (width = 0; tmp; tmp /= base)
		width++;

	/* Handle alternate hex form which prefixes 0x to non-zero values. */
	if (c->alt_form && base == 16 && c->val.unsignedint) {
		if (size > 0) {
			*buf++ = '0';
			size--;
		}
		if (size > 0) {
			*buf++ = c->capitalise ? 'X' : 'x';
			size--;
		}
		skip = 2;
	}

	/* Handle zero padding by setting the precision, if applicable. */
	if (c->lead_zeros && c->width > skip)
		c->precision = c->width - skip;

	/* Handle alternate octal form which forces the first digit to zero. */
	if (c->alt_form && base == 8) {
		if (c->val.unsignedint && width >= c->precision)
			width++;
	}

	if (width < c->precision)
		width = c->precision;

	for (size_t i = 0; i < width; i++) {
		char v = from_base(c->val.unsignedint % base);

		if (width-i <= size)
			buf[width-i-1] = c->capitalise ? toupper(v) : v;
		c->val.unsignedint /= base;
	}

	return skip+width;
}

static size_t render_signed(char *buf, size_t size, struct conv *conv)
{
	size_t len = 0;

	if (conv->val.signedint < 0) {
		if (size > len)
			buf[len] = '-';
		len++;
		conv->val.unsignedint = imaxabs(conv->val.signedint + 1);
		conv->val.unsignedint++;
	} else if (conv->always_sign || conv->pad_sign) {
		if (size > len)
			buf[len] = conv->always_sign ? '+' : ' ';
		len++;
	}

	if (size > len)
		len = render_unsigned(buf+len, size-len, len, 10, conv);
	else
		len = render_unsigned(NULL, 0, len, 10, conv);
	return len;
}

/*
 * Expand the field to its minimum field width, justifying appropriately.
 */
static size_t justify(char *buf, size_t size, size_t fill, struct conv *c)
{
	size_t a = fill, b = c->width;

	if (fill >= c->width)
		return fill;

	if (!c->left_justify) {
		for (size_t i = 0; i < fill; i++) {
			if (b-i <= size)
				buf[b-i-1] = buf[fill-i-1];
		}

		a -= fill;
		b -= fill;
	}

	assert(b > a);

	for (size_t i = a; i < b; i++) {
		if (i < size)
			buf[i] = ' ';
	}

	return fill + b - a;
}

static size_t render_conv(char *buf, size_t size, size_t pos, struct conv *c)
{
	size_t len;

	if (size > pos) {
		size -= pos;
		buf  += pos;
	} else {
		size = 0;
		buf  = NULL;
	}

	switch (c->specifier) {
	case CONV_PERCENT:
		if (size > 0)
			buf[0] = '%';
		len = 1;
		break;
	case CONV_SIGNED:
		len = render_signed(buf, size, c);
		break;
	case CONV_UNSIGNEDU:
		len = render_unsigned(buf, size, 0, 10, c);
		break;
	case CONV_UNSIGNEDO:
		len = render_unsigned(buf, size, 0, 8, c);
		break;
	case CONV_UNSIGNEDX:
		len = render_unsigned(buf, size, 0, 16, c);
		break;
	default:
		assert(!"unhandled conversion specifier");
	}

	return justify(buf, size, len, c);
}

int (vsnprintf)(char *str, size_t size, const char *fmt, va_list ap)
{
	size_t i, len = 0;
	struct conv conv;

	for (i = 0; fmt[i]; i++) {
		/* Handle the easy case first. */
		if (fmt[i] != '%') {
			if (size > len)
				str[len] = fmt[i];
			len += 1;
			continue;
		}

		i   += parse_conv(&fmt[i+1], &conv, ap);
		len += render_conv(str, size, len, &conv);
	}

	/* Zero-terminate the output if size is nonzero. */
	if (size > 0) {
		if (size > len)
			str[len] = 0;
		else
			str[size-1] = 0;
	}

	return len > INT_MAX ? -1 : len;
}
