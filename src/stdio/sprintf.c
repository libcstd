/*
 * Formatted input/output support.
 * Copyright (C) 2010 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdarg.h>

int (sprintf)(char *str, const char *fmt, ...)
{
	va_list ap;
	int rc;

	va_start(ap, fmt);
	rc = vsprintf(str, fmt, ap);
	va_end(ap);

	return rc;
}
