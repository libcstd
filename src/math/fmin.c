/*
 * fmin: floating point minimum function.
 * Copyright (C) 2010 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#include <cstd/mathcommon.h>

TYPE MF(fmin)(TYPE x, TYPE y)
{
	if (isless(x, y) || isnan(y))
		return x;
	if (isless(y, x) || isnan(x))
		return y;
	if (signbit(x))
		return x;
	return y;
}
