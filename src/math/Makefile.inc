noinst_LTLIBRARIES += src/math/libm.la src/math/libmf.la src/math/libml.la
libcstd_la_LIBADD  += src/math/libm.la src/math/libmf.la src/math/libml.la

src_math_libmf_la_CPPFLAGS = $(AM_CPPFLAGS) -DTYPE="float" -DSUFFIX=f
src_math_libmf_la_LDFLAGS  = $(src_math_libm_la_LDFLAGS)
src_math_libmf_la_SOURCES  = $(src_math_libm_la_SOURCES)

src_math_libml_la_CPPFLAGS = $(AM_CPPFLAGS) -DTYPE="long double" -DSUFFIX=l
src_math_libml_la_LDFLAGS  = $(src_math_libm_la_LDFLAGS)
src_math_libml_la_SOURCES  = $(src_math_libm_la_SOURCES)

src_math_libm_la_LDFLAGS =
src_math_libm_la_SOURCES = src/math/exp.c src/math/fmin.c src/math/fmax.c \
	src/math/nan.c src/math/fabs.c
