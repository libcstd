/*
 * nan: generate quiet NaNs, if supported.
 * Copyright (C) 2010 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#include <cstd/mathcommon.h>

TYPE MF(nan)(const char *tagp)
{
#ifdef NAN
	return NAN;
#else
	return 0;
#endif
}
