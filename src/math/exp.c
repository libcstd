/*
 * exp: exponential function.
 * Copyright (C) 2010 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#include <cstd/mathcommon.h>

TYPE MF(exp)(TYPE x)
{
	TYPE acc = 1, last = -1, curr = 1;

	if (x != x)
		return x;
	if (x < 0)
		return 1/MF(exp)(-x);

	for (TYPE i = 1; acc != last; i++) {
		last  = acc;
		curr *= x/i;
		acc  += curr;
	}

	return acc;
}
