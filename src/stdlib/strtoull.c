/*
 * strtoull: parse a string containing an encoding of an integer.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdlib.h>
#include <errno.h>
#include <limits.h>
#include <inttypes.h>

#include "intconv.h"

unsigned long long
(strtoull)(const char * restrict s, char ** restrict end, int base)
{
	uintmax_t val;
	int tmp, sign;

	if (end != NULL)
		*end = (char *)s;

	s = __intconv_sign(s, &sign);
	s = __intconv_base(s, &base);

	tmp   = errno;
	errno = 0;
	val   = _strtoumax(s, end, base);
	if (val > ULLONG_MAX || errno == ERANGE) {
		errno = ERANGE;
		return ULLONG_MAX;
	}

	errno = tmp;
	return sign * (unsigned long long)val;
}
