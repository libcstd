noinst_HEADERS += src/stdlib/intconv.h

libcstd_la_SOURCES += src/stdlib/abs.c src/stdlib/labs.c src/stdlib/llabs.c \
	src/stdlib/intconv.c src/stdlib/strtoimax.c src/stdlib/strtol.c \
	src/stdlib/strtoll.c src/stdlib/strtoumax.c src/stdlib/strtoul.c \
	src/stdlib/strtoull.c src/stdlib/atoi.c src/stdlib/atol.c \
	src/stdlib/atoll.c src/stdlib/imaxabs.c src/stdlib/imaxdiv.c \
	src/stdlib/div.c src/stdlib/ldiv.c src/stdlib/lldiv.c \
	src/stdlib/bsearch.c src/stdlib/abort.c

if WCHAR_SUPPORT
libcstd_la_SOURCES += src/stdlib/mbtowc.c src/stdlib/wctomb.c \
	src/stdlib/mblen.c
endif
