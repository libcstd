/*
 * bsearch: binary search of a sorted array.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdlib.h>

void *(bsearch)(const void *key, const void *_base, size_t nmemb, size_t size,
                int (*compar)(const void *, const void *))
{
	const unsigned char *base = _base;
	size_t l = 0, r = nmemb;

	while (l < r) {
		size_t c = l + (r - l)/2;
		int ord = compar(key, base + c*size);

		if (ord < 0)
			r = c;
		else if (ord > 0)
			l = c+1;
		else
			return (void *)(base + c*size);
	}

	return NULL;
}
