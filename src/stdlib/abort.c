/*
 * abort: abnormal program termination.
 * Copyright (C) 2010 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdlib.h>
#include <signal.h>

void (abort)(void)
{
	raise(SIGABRT);

	/* We still need to abort if an installed handler returns. */
	if (signal(SIGABRT, SIG_DFL) != SIG_ERR)
		raise(SIGABRT);

	/* Last-ditch effort to terminate the program. */
	while(1)
		_Exit(EXIT_FAILURE);
}
