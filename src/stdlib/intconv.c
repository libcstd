/*
 * Utilities for integer conversion functions.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <inttypes.h>

#include "intconv.h"

/* This sucks, but subtraction only works portably for digits. */
static int to_base(char c)
{
	switch (tolower(c)) {
	case '0': return  0;
	case '1': return  1;
	case '2': return  2;
	case '3': return  3;
	case '4': return  4;
	case '5': return  5;
	case '6': return  6;
	case '7': return  7;
	case '8': return  8;
	case '9': return  9;
	case 'a': return 10;
	case 'b': return 11;
	case 'c': return 12;
	case 'd': return 13;
	case 'e': return 14;
	case 'f': return 15;
	case 'g': return 16;
	case 'h': return 17;
	case 'i': return 18;
	case 'j': return 19;
	case 'k': return 20;
	case 'l': return 21;
	case 'm': return 22;
	case 'n': return 23;
	case 'o': return 24;
	case 'p': return 25;
	case 'q': return 26;
	case 'r': return 27;
	case 's': return 28;
	case 't': return 29;
	case 'u': return 30;
	case 'v': return 31;
	case 'w': return 32;
	case 'x': return 33;
	case 'y': return 34;
	case 'z': return 35;
	}

	return INT_MAX;
}

/*
 * Compute x*y + z using signed arithmetic, with overflow detection.
 * We assume that 2 <= y < 36 (the valid bases for strtol).
 * Similarly, 0 <= z < 36.
 *
 * Write x = 64 * x1 + x2, |x2| < 64.  Now x*y = 64*x1*y + x2*y.
 * Neither x1*y nor x0*y + z can overflow, since x0, y < 64.
 * Checking 64*x1*y for overflow is trivially |x1*y| <= |LONG_{MAX,MIN}/64|.
 * Addition overflow is then checked in the usual way.
 */
static int digit_signed(intmax_t *out, intmax_t x, int y, int z)
{
	intmax_t x1 = x/64, x0 = x%64;
	intmax_t z1 = x1*y, z0 = x0*y + z;

	if (z1 < 0 && z1 < INTMAX_MIN/64) {
		*out = INTMAX_MIN;
		return -ERANGE;
	} else if (z1 > 0 && z1 > INTMAX_MAX/64) {
		*out = INTMAX_MAX;
		return -ERANGE;
	}

	z1 *= 64;

	if (z1 < 0 && z1 < INTMAX_MIN - z0) {
		*out = INTMAX_MIN;
		return -ERANGE;
	} else if (z1 > 0 && z1 > INTMAX_MAX - z0) {
		*out = INTMAX_MAX;
		return -ERANGE;
	}

	*out = z1 + z0;
	return 0;
}

/*
 * Compute x*y + z using unsigned arithmetic, with overflow detection.
 * We assume that 2 <= y < 36 (the valid bases for strtol).
 * Similarly, 0 <= z < 36.
 */
static int digit_unsigned(uintmax_t *out, uintmax_t x, int y, int z)
{
	if (x * y < x) {
		*out = UINTMAX_MAX;
		return -ERANGE;
	}

	x *= y;

	if (x + z < x) {
		*out = UINTMAX_MAX;
		return -ERANGE;
	}

	*out = x + z;
	return 0;
}

const char *__intconv_sign(const char * restrict s, int * restrict sign)
{
	while (isspace((unsigned char)s[0]))
		s++;

	*sign = 1;
	switch (s[0]) {
	case '-':
		*sign = -1;
	case '+':
		s++;
		break;
	}

	return s;
}

const char *__intconv_base(const char * restrict s, int * restrict base)
{
	if ((*base == 0 || *base == 16) && s[0] == '0') {
		if (s[1] == 'x' || s[1] == 'X') {
			*base = 16;
			s += 2;
		} else if (*base == 0) {
			*base = 8;
			s += 1;
		}
	} else if (*base == 0) {
		*base = 10;
	}

	return s;
}

/* Numeric conversion helper for signed types. */
intmax_t
_strtoimax(const char * restrict s, char ** restrict end, int sign, int base)
{
	intmax_t val = 0;
	size_t i;

	for (i = 0; s[i] && to_base(s[i]) < base; i++) {
		if (digit_signed(&val, val, base, sign*to_base(s[i])) != 0) {
			errno = ERANGE;
		}
	}

	if (i > 0 && end != NULL)
		*end = (char *)s+i;
	return val;
}

/* Numeric conversion helper for unsigned types. */
uintmax_t
_strtoumax(const char * restrict s, char ** restrict end, int base)
{
	uintmax_t val = 0;
	size_t i;

	for (i = 0; s[i] && to_base(s[i]) < base; i++) {
		if (digit_unsigned(&val, val, base, to_base(s[i])) != 0) {
			errno = ERANGE;
		}
	}

	if (i > 0 && end != NULL)
		*end = (char *)s+i;
	return val;
}
