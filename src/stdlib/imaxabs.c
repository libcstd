/*
 * imaxabs: compute the absolute value of an intmax_t.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <inttypes.h>
#include <signal.h>

intmax_t (imaxabs)(intmax_t j)
{
	if (j < -INTMAX_MAX) {
		raise(SIGFPE);
		return j;
	}

	if (j < 0)
		return -j;
	return j;
}
