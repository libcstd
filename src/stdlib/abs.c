/*
 * abs: compute the absolute value of an int.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdlib.h>
#include <limits.h>
#include <signal.h>

int (abs)(int j)
{
	if (j < -INT_MAX) {
		raise(SIGFPE);
		return j;
	}

	if (j < 0)
		return -j;
	return j;
}
