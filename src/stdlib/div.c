/*
 * div: compute quotient and remainder.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdlib.h>

div_t (div)(int number, int denom)
{
	return (div_t){
		.quot = number/denom,
		.rem  = number%denom,
	};
}
