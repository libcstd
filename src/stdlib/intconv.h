#ifndef INTCONV_H_
#define INTCONV_H_

const char *__intconv_sign(const char * restrict s, int * restrict sign);
const char *__intconv_base(const char * restrict s, int * restrict base);

intmax_t
_strtoimax(const char * restrict s, char ** restrict end, int sign, int base);

uintmax_t
_strtoumax(const char * restrict s, char ** restrict end, int base);

#endif
