/*
 * imaxdiv: compute quotient and remainder.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <inttypes.h>

imaxdiv_t (imaxdiv)(intmax_t number, intmax_t denom)
{
	return (imaxdiv_t){
		.quot = number/denom,
		.rem  = number%denom,
	};
}
