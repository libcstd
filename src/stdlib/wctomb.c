/*
 * wctomb: convert a wide character to a multibyte character.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdlib.h>
#include <wchar.h>
#include <cstd/locale.h>

int (wctomb)(char *s, wchar_t wc)
{
	size_t rc;

	/* libcstd does not presently support any state-dependent encodings. */
	if (s == NULL)
		return 0;

	rc = __cstd_conv->encode(s, wc);
	if (rc == 0)
		return -1;
	return rc;
}
