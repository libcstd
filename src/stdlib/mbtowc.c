/*
 * mbtowc: convert a multibyte character to a wide character.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdlib.h>
#include <wchar.h>
#include <cstd/locale.h>

int (mbtowc)(wchar_t * restrict out, const char * restrict s, size_t n)
{
	wint_t wc;
	char *end;

	/* libcstd does not presently support any state-dependent encodings. */
	if (s == NULL)
		return 0;

	wc = __cstd_conv->decode(s, &end, n);
	if (wc == WEOF)
		return -1;
	if (out != NULL)
		*out = wc;
	if (wc == 0)
		return 0;
	return end - s;
}
