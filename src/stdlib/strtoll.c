/*
 * strtoll: parse a string containing an encoding of an integer.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdlib.h>
#include <errno.h>
#include <limits.h>
#include <inttypes.h>

long long (strtoll)(const char * restrict s, char ** restrict end, int base)
{
	intmax_t val = strtoimax(s, end, base);

	if (val > LLONG_MAX) {
		errno = ERANGE;
		return LLONG_MAX;
	} else if (val < LLONG_MIN) {
		errno = ERANGE;
		return LLONG_MIN;
	}

	return val;
}
