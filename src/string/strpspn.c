/*
 * strpspn: determine offset of the first character not matching a predicate.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 *
 * Not in C99.  The name "strpspn" is reserved for our use.
 */
#include <string.h>

size_t (strpspn)(const char *s, int (*p)(char, void *), void *data)
{
	size_t i;

	for (i = 0; s[i]; i++) {
		if (!p(s[i], data))
			break;
	}

	return i;
}
