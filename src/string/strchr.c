/*
 * strchr: search a string for a character.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <string.h>

char *(strchr)(const char *s, int c)
{
	for (size_t i = 0; s[i]; i++) {
		if (s[i] == (char)c)
			return (char *)&s[i];
	}

	return NULL;
}
