/*
 * memcpy: copy a memory region to a disjoint destination.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <string.h>

void *(memcpy)(void * restrict _s1, const void * restrict _s2, size_t n)
{
	const unsigned char *s2 = _s2;
	unsigned char *s1 = _s1;

	for (size_t i = 0; i < n; i++)
		s1[i] = s2[i];
	return s1;
}
