/*
 * strncmp: compare two null-padded, fixed-width fields.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <string.h>

int (strncmp)(const char *s1, const char *s2, size_t n)
{
	size_t i;

	for (i = 0; i < n && s1[i] && s2[i]; i++) {
		unsigned char c1 = *(unsigned char *)&s1[i];
		unsigned char c2 = *(unsigned char *)&s2[i];

		if (c1 < c2)
			return -1;
		if (c1 > c2)
			return  1;
	}

	if (s2[i])
		return -1;
	if (s1[i])
		return  1;
	return 0;
}
