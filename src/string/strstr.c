/*
 * strstr: search a string for a substring.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <string.h>

char *(strstr)(const char *s1, const char *s2)
{
	size_t n1 = strlen(s1), n2 = strlen(s2);
	size_t i, j;

	if (n1 < n2)
		return NULL;

	for (i = 0; i <= n1 - n2; i++) {
		for (j = 0; j < n2; j++) {
			if (s1[i+j] != s2[j])
				break;
		}

		if (!s2[j])
			return (char *)&s1[i];
	}

	return NULL;
}
