/*
 * strcspn: determine offset of the first character in a set.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <string.h>

size_t (strpspn)(const char *s, int (*p)(char, void *), void *data);

static int not_in(char c, void *_reject)
{
	const char *reject = _reject;

	for (size_t i = 0; reject[i]; i++) {
		if (c == reject[i])
			return 0;
	}

	return 1;
}

size_t (strcspn)(const char *s1, const char *s2)
{
	return strpspn(s1, not_in, (void *)s2);
}
