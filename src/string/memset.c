/*
 * memset: set all bytes in a memory region to a particular value.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <string.h>

void *(memset)(void *_s, int c, size_t n)
{
	unsigned char *s = _s;

	for (size_t i = 0; i < n; i++)
		s[i] = c;
	return s;
}
