/*
 * memchr: search a memory region for a character.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <string.h>

void *(memchr)(const void *_s, int c, size_t n)
{
	const unsigned char *s = _s;

	for (size_t i = 0; i < n; i++) {
		if (s[i] == (unsigned char)c)
			return (void *)&s[i];
	}

	return NULL;
}
