/*
 * strtok: extract tokens from a string.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <string.h>

char *(strtok)(char * restrict s1, const char * restrict s2)
{
	static char *pos;
	size_t len;

	if (s1)
		pos = s1;
	pos += strspn(pos, s2);
	len  = strcspn(pos, s2);

	if (len == 0)
		return NULL;

	s1   = pos;
	pos += len;

	if (*pos)
		*pos++ = 0;
	return s1;
}
