libcstd_la_SOURCES += src/string/memchr.c src/string/memcmp.c \
	src/string/memcpy.c src/string/memmove.c src/string/memset.c \
	src/string/strcat.c src/string/strchr.c src/string/strcmp.c \
	src/string/strcoll.c src/string/strcpy.c src/string/strcspn.c \
	src/string/strlen.c src/string/strncat.c src/string/strncmp.c \
	src/string/strncpy.c src/string/strpbrk.c src/string/strpspn.c \
	src/string/strrchr.c src/string/strspn.c src/string/strstr.c \
	src/string/strtok.c src/string/strxfrm.c
