/*
 * memmove: copy a memory region.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <string.h>

/*
 * Determine how two buffers overlap.  Pointer comparison is undefined for
 * buffers that do not overlap, so we need to use this hack.
 */
static int
compare_buf(const unsigned char *s1, const unsigned char *s2, size_t n)
{
	for (size_t i = 0; i < n; i++) {
		if (s1 + i == s2)
			return  1;
		if (s2 + i == s1)
			return -1;
	}

	return 0;
}

void *(memmove)(void *_s1, const void *_s2, size_t n)
{
	const unsigned char *s2 = _s2;
	unsigned char *s1 = _s1;

	if (compare_buf(s1, s2, n) < 0) {
		for (size_t i = 0; i < n; i++)
			s1[n-i-1] = s2[n-i-1];
	} else {
		for (size_t i = 0; i < n; i++)
			s1[i] = s2[i];
	}

	return s1;
}
