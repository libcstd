/*
 * strrchr: search a string for the last occurrence of a character.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <string.h>

char *(strrchr)(const char *s, int c)
{
	size_t n = strlen(s);

	for (size_t i = n; i <= n; i--) {
		if (s[i] == (char)c)
			return (char *)&s[i];
	}

	return NULL;
}
