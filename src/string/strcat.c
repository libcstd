/*
 * strcat: append a string to another.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <string.h>

char *(strcat)(char * restrict s1, const char * restrict s2)
{
	size_t i = strlen(s1);
	strcpy(s1 + i, s2);
	return s1;
}
