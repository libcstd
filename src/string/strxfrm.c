/*
 * strxfrm: transform a string according to locale.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <string.h>

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

size_t (strxfrm)(char * restrict s1, const char * restrict s2, size_t n)
{
	size_t len = strlen(s2);
	memcpy(s1, s2, MIN(len, n));
	return len;
}
