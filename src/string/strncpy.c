/*
 * strncpy: copy a null-padded, fixed-width field to another.
 * Copyright (C) 2009 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <string.h>

char *(strncpy)(char * restrict s1, const char * restrict s2, size_t n)
{
	for (size_t i = 0; i < n; i++) {
		if (*s2)
			s1[i] = *s2++;
		else
			s1[i] = 0;
	}

	return s1;
}
