/*
 * assert: program diagnostic tests.
 * Copyright (C) 2010 Nick Bowler.
 *
 * License LGPLv3+: GNU LGPL version 3 or later.  See COPYING.LIB for terms.
 * See <http://www.gnu.org/licenses/lgpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>

void (_assert)(const char *file, unsigned line,
               const char *func, const char *expr)
{
	fprintf(stderr, "%s:%u: %s: assertion failed: %s\n",
	                file, line, func, expr);
	abort();
}
