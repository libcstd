#ifndef _ASSERT_H_
#define _ASSERT_H_

static inline void
assert(const char *file, unsigned line, const char *func,
       _Bool failed, const char *expr)
{
	void _assert(const char *, unsigned, const char *, const char *);

	if (failed)
		_assert(file, line, func, expr);
}

#endif

#undef assert
#ifdef NDEBUG
#	define assert(expr) ((void)0)
#else
#	define assert(expr) assert(__FILE__, __LINE__, __func__, \
	                           0 == (expr), #expr)
#endif
