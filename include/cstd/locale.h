#ifndef _CSTD_LOCALE_H_
#define _CSTD_LOCALE_H_

#include <wchar.h>

struct __cstd_convfuncs {
	size_t maxlen;

	wint_t (*decode)
		(const char * restrict s, char ** restrict end, size_t n);
	size_t (*encode)
		(char *s, wint_t c);
};

extern struct __cstd_convfuncs *__cstd_conv;

#endif
