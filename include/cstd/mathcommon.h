#ifndef _CSTD_MATH_COMMON_H_
#define _CSTD_MATH_COMMON_H_

#include <math.h>

#define PASTE(a, b) a ## b
#define PASTE2(a, b) PASTE(a, b)
#define MF(func) (PASTE2(func, SUFFIX))

#ifndef TYPE
#define TYPE double
#endif

#ifndef SUFFIX
#define SUFFIX
#endif

#endif
