/*
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>

#undef strtol

#include "intconv.h"

int main(void)
{
	int rc = EXIT_SUCCESS;

	TEST_CONV_SANE(rc, long, strtol);

	TEST_CONV(rc, long, strtol, DECIMAL);
	TEST_CONV(rc, long, strtol, NEGATIVE);
	TEST_CONV(rc, long, strtol, OCTAL);
	TEST_CONV(rc, long, strtol, HEX0);
	TEST_CONV(rc, long, strtol, HEX1);
	TEST_CONV(rc, long, strtol, INVAL);
	TEST_CONV(rc, long, strtol, BASE15);
	TEST_CONV(rc, long, strtol, BASE36);

	TEST_CONV_EXT(rc, long, "%ld", 10, strtol, LONG_MIN);
	TEST_CONV_EXT(rc, long, "%ld", 10, strtol, LONG_MAX);

	TEST_CONV_OVER(rc, long, "%ld", 10, strtol, LONG_MIN);
	TEST_CONV_OVER(rc, long, "%ld", 10, strtol, LONG_MAX);

	return rc;
}
