/*
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#undef strcmp

int check_strcmp(const char *a, const char *b)
{
	int o1 = strcmp(a, b), o2 = strcmp(b, a);

	if (o1 < 0) o1 = -1;
	if (o1 > 0) o1 =  1;
	if (o2 < 0) o2 = -1;
	if (o2 > 0) o2 =  1;

	if ((o1 == 0 || o2 == 0) && o1 != o2) {
		puts("strcmp: equality not symmetric.");
		exit(EXIT_FAILURE);
	}

	if ((o1 != 0 || o2 != 0) && o1 == o2) {
		puts("strcmp: not a partial order.");
		exit(EXIT_FAILURE);
	}

	return o1;
}

int main(void)
{
	int rc = EXIT_SUCCESS;
	char sign1[] = { 1, 0 }, sign2[] = { -17, 0 };

	if (check_strcmp("alfalfa", "alfalfa") != 0) {
		puts("strcmp: string not equal to itself.");
		rc = EXIT_FAILURE;
	}
	if (check_strcmp("foo", "fooo") != -1) {
		puts("strcmp: string not greater than proper prefix.");
		rc = EXIT_FAILURE;
	}
	if (check_strcmp("", "foo") == 0) {
		puts("strcmp: empty string equal to non-empty string.");
		rc = EXIT_FAILURE;
	}
	if (check_strcmp("foo", "bar") == 0) {
		puts("strcmp: non-equal strings equal.");
		rc = EXIT_FAILURE;
	}
	if (check_strcmp("9", "0") != 1 || check_strcmp("154", "142") != 1) {
		puts("strcmp: incorrect digit ordering.");
		rc = EXIT_FAILURE;
	}
	if (check_strcmp(sign1, sign2) != -1) {
		puts("strcmp: incorrect ordering with negative chars.");
		rc = EXIT_FAILURE;
	}

	return rc;
}
