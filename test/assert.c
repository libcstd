/*
 * NDEBUG controls the meaning of <assert.h> being included.
 * <assert.h> can be included multiple times, each with or without NDEBUG.
 * The output requirements are printed to stderr, since the actual output
 * cannot be verified.
 *
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#include <assert.h>

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <setjmp.h>

jmp_buf env;

void sigabrt(int sig)
{
	longjmp(env, 1);
}

#define NDEBUG
#include <assert.h>

int skipped_assert(void)
{
	if (setjmp(env)) {
		puts("assert: aborted with NDEBUG defined.");
		return EXIT_FAILURE;
	}

	assert(0 == 1);
	return EXIT_SUCCESS;
}

#undef NDEBUG
#include <assert.h>

int main(void)
{
	char line[32];

	if (signal(SIGABRT, sigabrt) == SIG_ERR) {
		printf("signal: %s\n", strerror(errno));
		return EXIT_FAILURE;
	}

	/*
	 * Write the line number of the assert call into line.
	 * This _must_ occur before the setjmp call.
	 */
	sprintf(line, "%u", (unsigned)(__LINE__ + 7));

	printf("assert: next assertion message must contain these strings.\n");
	fprintf(stderr, "assert: %s, %s, %s, 0 == 1\n",
		__FILE__, line, __func__);

	if (!setjmp(env)) {
		assert(0 == 1);
		puts("assert: did not abort when NDEBUG not defined.");
		return EXIT_FAILURE;
	}

	if (setjmp(env)) {
		puts("assert: aborted on true expression.");
		return EXIT_FAILURE;
	}
	assert(1);

	return skipped_assert();
}
