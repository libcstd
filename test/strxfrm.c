/*
 * The first argument to strxfrm may be a null pointer if the length is zero.
 * The result of strcmp on transformed strings is the same as strcoll on the
 * original strings.
 * The return value should not be absurdly large.
 *
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#undef strxfrm
#undef strcoll
#undef strcmp

int main(void)
{
	char small[] = "Hello, world!", large[] = "A somewhat long string.";
	int rc = EXIT_SUCCESS;
	size_t n;

	n = strxfrm(NULL, small, 0);
	if (n > SIZE_MAX / 2) {
		puts("strxfrm: absurdly large return value.");
		rc = EXIT_FAILURE;
	}

	char smxfrm[n];
	if (strxfrm(smxfrm, small, sizeof smxfrm) != sizeof smxfrm) {
		puts("strxfrm: returned buffer size was a lie.");
		rc = EXIT_FAILURE;
	}

	n = strxfrm(NULL, large, 0);
	if (n > SIZE_MAX / 2) {
		puts("strxfrm: absurdly large return value.");
		rc = EXIT_FAILURE;
	}

	char lgxfrm[n];
	if (strxfrm(lgxfrm, large, sizeof lgxfrm) != sizeof lgxfrm) {
		puts("strxfrm: returned buffer size was a lie.");
		rc = EXIT_FAILURE;
	}

	if (rc == EXIT_SUCCESS) {
		int cmp  = strcmp(smxfrm, lgxfrm);
		int coll = strcoll(small, large);

		if (cmp  < 0) cmp  = -1;
		if (cmp  > 0) cmp  =  1;
		if (coll < 0) coll = -1;
		if (coll > 0) coll =  1;

		if (cmp != coll) {
			puts("strxfrm: wrong order of transformed strings.");
			rc = EXIT_FAILURE;
		}
	}

	return rc;
}
