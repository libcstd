/*
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>

#undef strtoul

#include "intconv.h"

int main(void)
{
	int rc = EXIT_SUCCESS;

	TEST_CONV_SANE(rc, unsigned long, strtoul);

	TEST_CONV(rc, unsigned long, strtoul, DECIMAL);
	TEST_CONV(rc, unsigned long, strtoul, NEGATIVE);
	TEST_CONV(rc, unsigned long, strtoul, OCTAL);
	TEST_CONV(rc, unsigned long, strtoul, HEX0);
	TEST_CONV(rc, unsigned long, strtoul, HEX1);
	TEST_CONV(rc, unsigned long, strtoul, INVAL);
	TEST_CONV(rc, unsigned long, strtoul, BASE15);
	TEST_CONV(rc, unsigned long, strtoul, BASE36);

	TEST_CONV_EXT (rc, unsigned long,   "%lu", 10, strtoul, ULONG_MAX);
	TEST_CONV_EXT (rc, unsigned long,   "%lo",  8, strtoul, ULONG_MAX);
	TEST_CONV_EXT (rc, unsigned long,   "%lx", 16, strtoul, ULONG_MAX);
	TEST_CONV_EXT (rc, unsigned long,   "%lX", 16, strtoul, ULONG_MAX);
	TEST_CONV_EXT (rc, unsigned long, "0X%lx",  0, strtoul, ULONG_MAX);

	TEST_CONV_EXT2(rc, unsigned long,  "-%lu",  0, strtoul, ULONG_MAX, 1);

	TEST_CONV_OVER(rc, unsigned long,   "%lu", 10, strtoul, ULONG_MAX);
	TEST_CONV_OVER(rc, unsigned long,   "%lo",  8, strtoul, ULONG_MAX);
	TEST_CONV_OVER(rc, unsigned long,   "%lx", 16, strtoul, ULONG_MAX);
	TEST_CONV_OVER(rc, unsigned long,   "%lX", 16, strtoul, ULONG_MAX);
	TEST_CONV_OVER(rc, unsigned long, "0x%lX",  0, strtoul, ULONG_MAX);
	TEST_CONV_OVER(rc, unsigned long,  "-%lu",  0, strtoul, ULONG_MAX);

	return rc;
}
