/*
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#undef strcspn

int main(void)
{
	char eps[] = "", str[] = "hello, world!";
	int rc = EXIT_SUCCESS;

	if (strcspn(str, eps) != sizeof str - 1) {
		puts("strcspn: incorrect length with empty reject set.");
		rc = EXIT_FAILURE;
	}
	if (strcspn(str, " ") != 6) {
		puts("strcspn: incorrect length with single rejection.");
		rc = EXIT_FAILURE;
	}
	if (strcspn(str, "\t \n") != 6) {
		puts("strcspn: incorrect length with multiple rejection.");
		rc = EXIT_FAILURE;
	}
	if (strcspn(str, " wh") != 0) {
		puts("strcspn: incorrect length with first char rejected.");
		rc = EXIT_FAILURE;
	}
	if (strcspn(eps, str) != 0) {
		puts("strcspn: prefix of empty string with non-zero length.");
		rc = EXIT_FAILURE;
	}

	return rc;
}
