/*
 * The example in ISO/IEC 9899:1999 must work.
 *
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#undef strtok
#undef strcmp

int main(void)
{
	char str[] = "?a???b,,,#c", *tok;
	int rc = EXIT_SUCCESS;

	tok = strtok(str, "?");
	if (tok == NULL || strcmp(tok, "a") != 0) {
		puts("strtok: incorrect first token.");
		rc = EXIT_FAILURE;
	}
	tok = strtok(NULL, ",");
	if (tok == NULL || strcmp(tok, "??b") != 0) {
		puts("strtok: incorrect second token.");
		rc = EXIT_FAILURE;
	}
	tok = strtok(NULL, "#,");
	if (tok == NULL || strcmp(tok, "c") != 0) {
		puts("strtok: incorrect third token.");
		rc = EXIT_FAILURE;
	}
	if (strtok(NULL, "?") != NULL) {
		puts("strtok: extra tokens in string.");
		rc = EXIT_FAILURE;
	}

	return rc;
}
