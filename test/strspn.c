/*
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#undef strspn

int main(void)
{
	char eps[] = "", str[] = "hello, world!";
	int rc = EXIT_SUCCESS;

	if (strspn(str, eps) != 0) {
		puts("strspn: incorrect length with empty accept set.");
		rc = EXIT_FAILURE;
	}
	if (strspn(str, " wh") != 1) {
		puts("strspn: incorrect length with single acceptance.");
		rc = EXIT_FAILURE;
	}
	if (strspn(str, "\te \n") != 0) {
		puts("strspn: incorrect length with first char accepted.");
		rc = EXIT_FAILURE;
	}
	if (strspn(str, str) != sizeof str - 1) {
		puts("strspn: incorrect length with total acceptance.");
		rc = EXIT_FAILURE;
	}

	return rc;
}
