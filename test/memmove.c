/*
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#undef memmove

static unsigned char buf[4096];

int main(void)
{
	unsigned char *buf1 = buf+1473, *buf2 = buf + 1758;
	int i, rc = EXIT_SUCCESS;

	memset(buf, 0x5a, sizeof buf);
	for (i = 0; i < 2048; i++)
		buf1[i] = i;

	memmove(buf1, buf1, 2048);

	for (i = 0; i < 2048; i++) {
		if (buf1[i] != (unsigned char)i) {
			puts("memmove: corrupted data moving nowhere.");
			rc = EXIT_FAILURE;
			break;
		}
	}

	memset(buf, 0x5a, sizeof buf);
	for (i = 0; i < 2048; i++)
		buf2[i] = i;

	memmove(buf1, buf2, 2048);

	for (i = 0; i < 2048; i++) {
		if (buf1[i] != (unsigned char)i) {
			puts("memmove: corrupted data moving ``left''.");
			rc = EXIT_FAILURE;
			break;
		}
	}

	memset(buf, 0x5a, sizeof buf);
	for (i = 0; i < 2048; i++)
		buf1[i] = i;

	memmove(buf2, buf1, 2048);

	for (i = 0; i < 2048; i++) {
		if (buf2[i] != (unsigned char)i) {
			puts("memmove: corrupted data moving ``right''.");
			rc = EXIT_FAILURE;
			break;
		}
	}

	memset(buf, 0x5a, sizeof buf);
	for (i = 0; i < 1024; i++)
		buf[i] = buf2[i] = i;

	memmove(buf, buf2, 1024);

	for (i = 0; i < 1024; i++) {
		if (buf[i] != (unsigned char)i) {
			puts("memmove: corrupted disjoint destination.");
			rc = EXIT_FAILURE;
			break;
		}
		if (buf2[i] != (unsigned char)i) {
			puts("memmove: corrupted disjoint source.");
			rc = EXIT_FAILURE;
			break;
		}
	}

	return rc;
}
