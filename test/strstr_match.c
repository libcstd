/*
 * Check that strstr actually finds substrings.
 * Every string is a substring of itself.
 * Every proper prefix/suffix is a substring.
 * A string is not a substring of its proper substrings.
 *
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#undef strstr

int main(void)
{
	char alf[] = "alfalfa", big[] = "I like alfalfa on hamburgers.";
	int rc = EXIT_SUCCESS;

	if (strstr(alf, alf) != alf) {
		puts("strstr: string not found in itself.");
		rc = EXIT_FAILURE;
	}
	if (strstr(big, alf) != big + 7) {
		puts("strstr: string not found in larger string.");
		rc = EXIT_FAILURE;
	}
	if (strstr(alf, "alfa") != alf) {
		puts("strstr: prefix not found in string.");
		rc = EXIT_FAILURE;
	}
	if (strstr(alf, "falfa") != alf + 2) {
		puts("strstr: suffix not found in string.");
		rc = EXIT_FAILURE;
	}
	if (strstr(alf, "alfalfal") != NULL) {
		puts("strstr: string found in proper prefix.");
		rc = EXIT_FAILURE;
	}
	if (strstr("lfalfa", alf) != NULL) {
		puts("strstr: string found in proper suffix.");
		rc = EXIT_FAILURE;
	}
	if (strstr(alf, big) != NULL) {
		puts("strstr: large string found in smaller string.");
		rc = EXIT_FAILURE;
	}

	return rc;
}
