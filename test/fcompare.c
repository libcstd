/*
 * Copyright (C) 2010 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#include "mathutil.h"

#undef fmin
#undef fmax

#define MINMAX_SIGN(func, op, x, y) do { \
	if (op signbit(func(x, y))) { \
		puts(#func ": warning: does not honour negative zeros."); \
		return; \
	} \
} while(0)

/*
 * The min/max functions are not required in C to differentiate between zero
 * and negative zero, but since libcstd tries to, it should be tested.
 * Print a warning if they don't.
 */
void check_negative_zero(void)
{
	if (signbit(-0.0) == signbit(0.0))
		return;

	MINMAX_SIGN(fmin,  !, -0.0,  0.0);
	MINMAX_SIGN(fminf, !, -0.0,  0.0);
	MINMAX_SIGN(fminl, !, -0.0,  0.0);
	MINMAX_SIGN(fmin,  !,  0.0, -0.0);
	MINMAX_SIGN(fminf, !,  0.0, -0.0);
	MINMAX_SIGN(fminl, !,  0.0, -0.0);

	MINMAX_SIGN(fmax,   , -0.0,  0.0);
	MINMAX_SIGN(fmaxf,  , -0.0,  0.0);
	MINMAX_SIGN(fmaxl,  , -0.0,  0.0);
	MINMAX_SIGN(fmax,   ,  0.0, -0.0);
	MINMAX_SIGN(fmaxf,  ,  0.0, -0.0);
	MINMAX_SIGN(fmaxl,  ,  0.0, -0.0);
}

int main(void)
{
	MATH_EXACT_ALL(fmin,-1.0, -1.0,  0.0);
	MATH_EXACT_ALL(fmin,-1.0,  0.0, -1.0);
	MATH_EXACT_ALL(fmin, 0.0,  0.0,  1.0);
	MATH_EXACT_ALL(fmin, 0.0,  1.0,  0.0);

	MATH_EXACT_ALL(fmax, 0.0, -1.0,  0.0);
	MATH_EXACT_ALL(fmax, 0.0,  0.0, -1.0);
	MATH_EXACT_ALL(fmax, 1.0,  0.0,  1.0);
	MATH_EXACT_ALL(fmax, 1.0,  1.0,  0.0);

#ifdef NAN
	MATH_EXACT_ALL(fmin, 0.0, NAN, 0.0);
	MATH_EXACT_ALL(fmin, 0.0, 0.0, NAN);
	MATH_EXACT(fmin,  HUGE_VAL,  NAN, HUGE_VAL);
	MATH_EXACT(fminf, HUGE_VALF, NAN, HUGE_VALF);
	MATH_EXACT(fminl, HUGE_VALL, NAN, HUGE_VALL);
	MATH_EXACT(fmin,  HUGE_VAL,  HUGE_VAL,  NAN);
	MATH_EXACT(fminf, HUGE_VALF, HUGE_VALF, NAN);
	MATH_EXACT(fminl, HUGE_VALL, HUGE_VALL, NAN);

	MATH_EXACT_ALL(fmax, 0.0, NAN, 0.0);
	MATH_EXACT_ALL(fmax, 0.0, 0.0, NAN);
	MATH_EXACT(fmax,  HUGE_VAL,  NAN, HUGE_VAL);
	MATH_EXACT(fmaxf, HUGE_VALF, NAN, HUGE_VALF);
	MATH_EXACT(fmaxl, HUGE_VALL, NAN, HUGE_VALL);
	MATH_EXACT(fmax,  HUGE_VAL,  HUGE_VAL,  NAN);
	MATH_EXACT(fmaxf, HUGE_VALF, HUGE_VALF, NAN);
	MATH_EXACT(fmaxl, HUGE_VALL, HUGE_VALL, NAN);

	MATH_EXACT_ALL(fmin, NAN, NAN, NAN);
	MATH_EXACT_ALL(fmax, NAN, NAN, NAN);
#endif

	check_negative_zero();

	return 0;
}
