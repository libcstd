/*
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <inttypes.h>

#undef strtoimax

#include "intconv.h"

int main(void)
{
	int rc = EXIT_SUCCESS;

	TEST_CONV_SANE(rc, intmax_t, strtoimax);

	TEST_CONV(rc, intmax_t, strtoimax, DECIMAL);
	TEST_CONV(rc, intmax_t, strtoimax, NEGATIVE);
	TEST_CONV(rc, intmax_t, strtoimax, OCTAL);
	TEST_CONV(rc, intmax_t, strtoimax, HEX0);
	TEST_CONV(rc, intmax_t, strtoimax, HEX1);
	TEST_CONV(rc, intmax_t, strtoimax, INVAL);
	TEST_CONV(rc, intmax_t, strtoimax, BASE15);
	TEST_CONV(rc, intmax_t, strtoimax, BASE36);

	TEST_CONV_EXT(rc, intmax_t,  "%jd", 10, strtoimax, INTMAX_MIN);
	TEST_CONV_EXT(rc, intmax_t,  "%jd", 10, strtoimax, INTMAX_MAX);

	TEST_CONV_OVER(rc, intmax_t, "%jd", 10, strtoimax, INTMAX_MIN);
	TEST_CONV_OVER(rc, intmax_t, "%jd", 10, strtoimax, INTMAX_MAX);

	return rc;
}
