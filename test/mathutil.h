#ifndef TEST_MATHUTIL_H_
#define TEST_MATHUTIL_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MATH_EXACT_INTERNAL(str, func, result, ...) do { \
	if (isnan(result)) { \
		if (!isnan((func)(__VA_ARGS__))) { \
			puts(str ": result is not NaN"); \
			return EXIT_FAILURE; \
		} \
	} else if ((func)(__VA_ARGS__) != (result)) { \
		printf(str ": result is not %Lg\n", (long double)(result)); \
		return EXIT_FAILURE; \
	} \
} while (0)

#define MATH_EXACT(func, result, ...) MATH_EXACT_INTERNAL( \
	#func "(" #__VA_ARGS__ ")", func, result, __VA_ARGS__)

#define MATH_EXACT_ALL(func, result, ...) do { \
	MATH_EXACT_INTERNAL(#func "(" #__VA_ARGS__ ")", \
		func, result, __VA_ARGS__); \
	MATH_EXACT_INTERNAL(#func "f(" #__VA_ARGS__ ")", \
		func ## f, result, __VA_ARGS__); \
	MATH_EXACT_INTERNAL(#func "l(" #__VA_ARGS__ ")", \
		func ## l, result, __VA_ARGS__); \
} while (0)

#endif
