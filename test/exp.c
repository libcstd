/*
 * Copyright (C) 2010 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#include "mathutil.h"

#undef exp
#undef expf
#undef expl

int main(void)
{
	MATH_EXACT_ALL(exp, 1.0,  0.0);
	MATH_EXACT_ALL(exp, 1.0, -0.0);

#ifdef __STDC_IEC_559__
	MATH_EXACT_ALL(exp, INFINITY, INFINITY);
	MATH_EXACT_ALL(exp, 0.0,     -INFINITY);
#endif

	return 0;
}
