/*
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#undef strlen

int main(void)
{
	char eps[] = "", sing[] = "x", mult[] = "a somewhat long string";
	int rc = EXIT_SUCCESS;

	if (strlen(eps) != 0) {
		puts("strlen: non-zero length of empty string.");
		rc = EXIT_FAILURE;
	}
	if (strlen(sing) != 1) {
		puts("strlen: non-unit length of singleton string.");
		rc = EXIT_FAILURE;
	}
	if (strlen(mult) != sizeof mult - 1) {
		puts("strlen: incorrect length of long string.");
		rc = EXIT_FAILURE;
	}

	return rc;
}
