/*
 * The empty string is a substring of any string, including the empty string.
 * Any non-empty string is not a substring of the empty string.
 *
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#undef strstr

int main(void)
{
	char eps[] = "", sing[] = "x", mult[] = "a somewhat long string";
	int rc = EXIT_SUCCESS;

	if (strstr(eps, eps) != eps) {
		puts("strstr: empty string not found in empty string.");
		rc = EXIT_FAILURE;
	}
	if (strstr(sing, eps) != sing) {
		puts("strstr: empty string not found in singleton string.");
		rc = EXIT_FAILURE;
	}
	if (strstr(mult, eps) != mult) {
		puts("strstr: empty string not found in long string.");
		rc = EXIT_FAILURE;
	}
	if (strstr(eps, sing) != NULL) {
		puts("strstr: singleton string found in empty string.");
		rc = EXIT_FAILURE;
	}
	if (strstr(eps, mult) != NULL) {
		puts("strstr: long string found in empty string.");
		rc = EXIT_FAILURE;
	}

	return rc;
}
