/*
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <inttypes.h>

#undef strtoumax

#include "intconv.h"

int main(void)
{
	int rc = EXIT_SUCCESS;

	TEST_CONV_SANE(rc, uintmax_t, strtoumax);

	TEST_CONV(rc, uintmax_t, strtoumax, DECIMAL);
	TEST_CONV(rc, uintmax_t, strtoumax, NEGATIVE);
	TEST_CONV(rc, uintmax_t, strtoumax, OCTAL);
	TEST_CONV(rc, uintmax_t, strtoumax, HEX0);
	TEST_CONV(rc, uintmax_t, strtoumax, HEX1);
	TEST_CONV(rc, uintmax_t, strtoumax, INVAL);
	TEST_CONV(rc, uintmax_t, strtoumax, BASE15);
	TEST_CONV(rc, uintmax_t, strtoumax, BASE36);

	TEST_CONV_EXT (rc, uintmax_t,   "%ju", 10, strtoumax, UINTMAX_MAX);
	TEST_CONV_EXT (rc, uintmax_t,   "%jo",  8, strtoumax, UINTMAX_MAX);
	TEST_CONV_EXT (rc, uintmax_t,   "%jx", 16, strtoumax, UINTMAX_MAX);
	TEST_CONV_EXT (rc, uintmax_t,   "%jX", 16, strtoumax, UINTMAX_MAX);
	TEST_CONV_EXT (rc, uintmax_t, "0X%jx",  0, strtoumax, UINTMAX_MAX);

	TEST_CONV_EXT2(rc, uintmax_t,  "-%ju",  0, strtoumax, UINTMAX_MAX, 1);

	TEST_CONV_OVER(rc, uintmax_t,   "%ju", 10, strtoumax, UINTMAX_MAX);
	TEST_CONV_OVER(rc, uintmax_t,   "%jo",  8, strtoumax, UINTMAX_MAX);
	TEST_CONV_OVER(rc, uintmax_t,   "%jx", 16, strtoumax, UINTMAX_MAX);
	TEST_CONV_OVER(rc, uintmax_t,   "%jX", 16, strtoumax, UINTMAX_MAX);
	TEST_CONV_OVER(rc, uintmax_t, "0x%jX",  0, strtoumax, UINTMAX_MAX);
	TEST_CONV_OVER(rc, uintmax_t,  "-%ju",  0, strtoumax, UINTMAX_MAX);

	return rc;
}
