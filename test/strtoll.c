/*
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>

#undef strtoll

#include "intconv.h"

int main(void)
{
	int rc = EXIT_SUCCESS;

	TEST_CONV_SANE(rc, long long, strtoll);

	TEST_CONV(rc, long long, strtoll, DECIMAL);
	TEST_CONV(rc, long long, strtoll, NEGATIVE);
	TEST_CONV(rc, long long, strtoll, OCTAL);
	TEST_CONV(rc, long long, strtoll, HEX0);
	TEST_CONV(rc, long long, strtoll, HEX1);
	TEST_CONV(rc, long long, strtoll, INVAL);
	TEST_CONV(rc, long long, strtoll, BASE15);
	TEST_CONV(rc, long long, strtoll, BASE36);

	TEST_CONV_EXT(rc, long long, "%lld", 10, strtoll, LLONG_MIN);
	TEST_CONV_EXT(rc, long long, "%lld", 10, strtoll, LLONG_MAX);

	TEST_CONV_OVER(rc, long long, "%lld", 10, strtoll, LLONG_MIN);
	TEST_CONV_OVER(rc, long long, "%lld", 10, strtoll, LLONG_MAX);

	return rc;
}
