/*
 * Copyright (C) 2010 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <limits.h>

static int check(const char *expect, const char *fmt, ...)
{
	static char buf[1024];
	va_list ap;
	int rc;

	va_start(ap, fmt);
	rc = vsprintf(buf, fmt, ap);
	va_end(ap);

	if (strcmp(expect, buf) != 0) {
		puts("vsprintf: output did not match the expected value.");
		printf("       got: \"%s\"\n", buf);
		printf("  expected: \"%s\"\n", expect);
		return -1;
	}

	if (rc != strlen(expect)) {
		puts("vsprintf: return value did not match expectations.");
		return -1;
	}

	return 0;
}

#define CHECK_SIMPLE(expect, ...) if (check(expect, __VA_ARGS__)) { \
	puts("sprintf(" #__VA_ARGS__ ") failed."); \
	return EXIT_FAILURE; \
}

int main(void)
{
	/* Sanity check */
	CHECK_SIMPLE("%",    "%%");
	CHECK_SIMPLE("0",    "%d", 0);
	CHECK_SIMPLE("42",   "%d", 42);
	CHECK_SIMPLE("42",   "%u", 42u);
	CHECK_SIMPLE("1777", "%o", 0x3ffu);
	CHECK_SIMPLE("3ff",  "%x", 0x3ffu);

	/* Minimum field widths */
	CHECK_SIMPLE("[  42]", "[%4d]",   42);
	CHECK_SIMPLE("[  42]", "[% 4d]",  42);
	CHECK_SIMPLE("[ -42]", "[%4d]",  -42);
	CHECK_SIMPLE("[0042]", "[%04d]",  42);
	CHECK_SIMPLE("[+042]", "[%+04d]", 42);
	CHECK_SIMPLE("[ 042]", "[% 04d]", 42);
	CHECK_SIMPLE("[ 42 ]", "[% -4d]", 42);
	CHECK_SIMPLE("[42  ]", "[%-4d]",  42);
	CHECK_SIMPLE("[42  ]", "[%-04d]", 42);

	/* Integer precision */
	CHECK_SIMPLE("",       "%.d",   0);
	CHECK_SIMPLE("0000",   "%.4d",  0);
	CHECK_SIMPLE("  0042", "%6.4d", 42);

	/* Integer alternate forms */
	CHECK_SIMPLE("0",    "%#o",   0u);
	CHECK_SIMPLE("0",    "%#x",   0u);
	CHECK_SIMPLE("01",   "%#o",   1u);
	CHECK_SIMPLE("01",   "%#.2o", 1u);
	CHECK_SIMPLE("0x01", "%#.2x", 1u);
	CHECK_SIMPLE("0x01", "%#04x", 1u);

	/* Mutually exclusive options */
	CHECK_SIMPLE("0    ", "%-05d",    0);
	CHECK_SIMPLE("    0", "%05.1d",   0);
	CHECK_SIMPLE("+0",    "%+ d",     0);
	CHECK_SIMPLE("+    ", "%0- +5.d", 0);

	/* Narrow types shall be converted prior to printing */
	CHECK_SIMPLE("0", "%hhu", UCHAR_MAX+1);
	CHECK_SIMPLE("0", "%hu",  USHRT_MAX+1);

	return 0;
}
