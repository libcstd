/*
 * Helper macros for test cases of the strtol family of functions.
 * Copyright (C) 2009 Nick Bowler.
 *
 * This program is free software, licensed under the terms of the GNU General
 * Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version.
 */

#ifndef INTCONV_H_
#define INTCONV_H_

#define DECIMAL_STR      "1234 "
#define DECIMAL_VAL      1234
#define DECIMAL_END      4
#define DECIMAL_BASE     0
#define DECIMAL_VALFAIL  "incorrect parse of base-0 1234."
#define DECIMAL_ENDFAIL  "incorrect endptr parsing base-0 1234."

#define NEGATIVE_STR     "-42"
#define NEGATIVE_VAL     -42
#define NEGATIVE_END     3
#define NEGATIVE_BASE    0
#define NEGATIVE_VALFAIL "incorrect parse of base-0 -42."
#define NEGATIVE_ENDFAIL "incorrect endptr parsing base-0 -42."

#define OCTAL_STR        "\t\n  012348012"
#define OCTAL_VAL        01234
#define OCTAL_END        9
#define OCTAL_BASE       0
#define OCTAL_VALFAIL    "incorrect parse of base-0 012348012."
#define OCTAL_ENDFAIL    "incorrect endptr parsing base-0 012348012."

#define HEX0_STR         " 0xfaceoff "
#define HEX0_VAL         0xface
#define HEX0_END         7
#define HEX0_BASE        0
#define HEX0_VALFAIL     "incorrect parse of base-0 0xfaceoff."
#define HEX0_ENDFAIL     "incorrect endptr parsing base-0 0xfaceoff."

#define HEX1_STR         "0Xfiretruck"
#define HEX1_VAL         0xf
#define HEX1_END         3
#define HEX1_BASE        16
#define HEX1_VALFAIL     "incorrect parse of base-16 0Xfiretruck."
#define HEX1_ENDFAIL     "incorrect endptr parsing base-16 0xfiretruck."

#define INVAL_STR        "    abcd"
#define INVAL_VAL        0
#define INVAL_END        0
#define INVAL_BASE       0
#define INVAL_VALFAIL    "did not return 0 on invalid input."
#define INVAL_ENDFAIL    "incorrect endptr on invalid input."

#define BASE15_STR       "1a2B"
#define BASE15_VAL       5666
#define BASE15_END       4
#define BASE15_BASE      15
#define BASE15_VALFAIL   "incorrect parse of base-15 1a2B."
#define BASE15_ENDFAIL   "incorrect endptr on base-15 1a2B."

#define BASE36_STR       "zOMg"
#define BASE36_VAL       1664872
#define BASE36_END       4
#define BASE36_BASE      36
#define BASE36_VALFAIL   "incorrect parse of base-36 zOMg."
#define BASE36_ENDFAIL   "incorrect endptr on base-36 zOMg."

/* Wrapper macro which generates tests from the above descriptions. */
#define TEST_CONV(rc, type, parser, test) do { \
	char *str = test ## _STR, *end = NULL; \
	type val; \
\
	errno = -1; \
	val = parser(str, &end, test ## _BASE); \
	if (errno == 0 || errno == ERANGE) { \
		puts(#parser ": errno was modified unexpectedly."); \
		rc = EXIT_FAILURE; \
	} \
	if (val != test ## _VAL) { \
		puts(#parser ":" test ## _VALFAIL); \
		rc = EXIT_FAILURE; \
	} \
	if (end - str != test ## _END) { \
		puts(#parser ":" test ## _ENDFAIL); \
		rc = EXIT_FAILURE; \
	} \
} while(0)

/* Generate interface sanity test. */
#define TEST_CONV_SANE(rc, type, parser) do { \
	char *eps = "", *end = NULL; \
	type val; \
\
	if (parser(eps, NULL, 10) != 0) { \
		puts(#parser ": non-zero return on empty input."); \
		rc = EXIT_FAILURE; \
	} \
	if (parser("42", NULL, 10) != 42) { \
		puts(#parser ": incorrect parse with NULL endptr."); \
		rc = EXIT_FAILURE; \
	} \
\
	val = strtol(eps, &end, 0); \
	if (eps != end) { \
		puts(#parser ": invalid endptr on empty input."); \
		rc = EXIT_FAILURE; \
	} \
} while(0)

/* Internal use */
#define TEST_CONV_EXT_HEAD_(rc, type, fmt, base, parser, ext) \
	char str[64], *end = NULL; \
	type val; \
\
	if (snprintf(str, sizeof str, fmt, ext) >= sizeof str) { \
		puts(#parser ": recompile with a larger buffer."); \
		abort(); \
	} \
\
	errno = -1; \
	val = parser(str, &end, base);

/* Generate a test for extreme values. */
#define TEST_CONV_EXT2(rc, type, fmt, base, parser, ext, want) do { \
	TEST_CONV_EXT_HEAD_(rc, type, fmt, base, parser, ext) \
	if (errno == 0 || errno == ERANGE) { \
		puts(#parser ": errno was modified unexpectedly."); \
		rc = EXIT_FAILURE; \
	} \
	if (val != want) { \
		puts(#parser ": incorrect parse of extreme value."); \
		rc = EXIT_FAILURE; \
	} \
	if (*end != 0) { \
		puts(#parser ": incorrect endptr on extreme value."); \
		rc = EXIT_FAILURE; \
	} \
} while(0)

#define TEST_CONV_EXT(rc, type, fmt, base, parser, ext) \
	TEST_CONV_EXT2(rc, type, fmt, base, parser, ext, ext)

/* Generate a test for overflow values. */
#define TEST_CONV_OVER(rc, type, fmt, base, parser, ext) do { \
	TEST_CONV_EXT_HEAD_(rc, type, fmt "111", base, parser, ext) \
	if (errno != ERANGE) { \
		puts(#parser ": errno was unmodified on overflow."); \
		rc = EXIT_FAILURE; \
	} \
	if (val != ext) { \
		puts(#parser ": incorrect parse of overflow value."); \
		rc = EXIT_FAILURE; \
	} \
	if (*end != 0) { \
		puts(#parser ": incorrect endptr on overflow value."); \
		rc = EXIT_FAILURE; \
	} \
} while(0)

#endif
