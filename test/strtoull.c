/*
 * Copyright (C) 2009 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>

#undef strtoull

#include "intconv.h"


int main(void)
{
	typedef unsigned long long ullong;
	int rc = EXIT_SUCCESS;

	TEST_CONV_SANE(rc, ullong, strtoull);

	TEST_CONV(rc, ullong, strtoull, DECIMAL);
	TEST_CONV(rc, ullong, strtoull, NEGATIVE);
	TEST_CONV(rc, ullong, strtoull, OCTAL);
	TEST_CONV(rc, ullong, strtoull, HEX0);
	TEST_CONV(rc, ullong, strtoull, HEX1);
	TEST_CONV(rc, ullong, strtoull, INVAL);
	TEST_CONV(rc, ullong, strtoull, BASE15);
	TEST_CONV(rc, ullong, strtoull, BASE36);

	TEST_CONV_EXT (rc, ullong,   "%llu", 10, strtoull, ULLONG_MAX);
	TEST_CONV_EXT (rc, ullong,   "%llo",  8, strtoull, ULLONG_MAX);
	TEST_CONV_EXT (rc, ullong,   "%llx", 16, strtoull, ULLONG_MAX);
	TEST_CONV_EXT (rc, ullong,   "%llX", 16, strtoull, ULLONG_MAX);
	TEST_CONV_EXT (rc, ullong, "0X%llx",  0, strtoull, ULLONG_MAX);

	TEST_CONV_EXT2(rc, ullong,  "-%llu",  0, strtoull, ULLONG_MAX, 1);

	TEST_CONV_OVER(rc, ullong,   "%llu", 10, strtoull, ULLONG_MAX);
	TEST_CONV_OVER(rc, ullong,   "%llo",  8, strtoull, ULLONG_MAX);
	TEST_CONV_OVER(rc, ullong,   "%llx", 16, strtoull, ULLONG_MAX);
	TEST_CONV_OVER(rc, ullong,   "%llX", 16, strtoull, ULLONG_MAX);
	TEST_CONV_OVER(rc, ullong, "0x%llX",  0, strtoull, ULLONG_MAX);
	TEST_CONV_OVER(rc, ullong,  "-%llu",  0, strtoull, ULLONG_MAX);

	return rc;
}
