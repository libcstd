/*
 * Copyright (C) 2009-2010 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>

#undef bsearch

#define ARRAY_SIZE(arr) (sizeof (arr) / sizeof (arr)[0])

/*
 * Sorted array of strings.  We use only digit strings because they have a
 * consistent lexicographic ordering across implementations.  Since they're
 * all three digits, the lexicographic ordering matches the numeric one.
 */
static char arr[100][4] = {
	"007", "011", "016", "017", "019", "033", "039", "053", "056", "061",
	"063", "064", "065", "111", "130", "148", "151", "152", "156", "158",
	"169", "176", "184", "194", "204", "205", "211", "228", "231", "232",
	"245", "258", "268", "269", "282", "284", "288", "290", "302", "321",
	"329", "335", "346", "357", "366", "369", "370", "387", "395", "402",
	"404", "414", "417", "439", "442", "458", "462", "466", "512", "537",
	"548", "555", "560", "562", "569", "574", "583", "594", "595", "612",
	"622", "635", "639", "645", "648", "655", "659", "721", "732", "751",
	"760", "763", "779", "785", "789", "801", "839", "884", "903", "924",
	"925", "939", "956", "957", "959", "965", "976", "980", "984", "999",
};

static int str_cmp(const void *a, const void *b)
{
	return strcmp(a, b);
}

static jmp_buf err_jmp;

static int error_cmp(const void *a, const void *b)
{
	longjmp(err_jmp, 1);
}

int main(void)
{
	int rc = EXIT_SUCCESS;
	char *str;

	str = bsearch("007", arr, 1, sizeof arr[0], str_cmp);
	if (!str || strcmp(str, "007") != 0) {
		puts("bsearch: failed to find element in singleton array.");
		rc = EXIT_FAILURE;
	}

	str = bsearch("011", arr, 1, sizeof arr[0], str_cmp);
	if (str) {
		puts("bsearch: found an element not in singleton array.");
		rc = EXIT_FAILURE;
	}

	if (!setjmp(err_jmp)) {
		str = bsearch("007", arr, 0, sizeof arr[0], error_cmp);
		if (str) {
			puts("bsearch: found element in zero-length array.");
			rc = EXIT_FAILURE;
		}
	} else {
		puts("bsearch: comparison on zero-length array");
		rc = EXIT_FAILURE;
	}

	str = bsearch("335", arr, ARRAY_SIZE(arr), sizeof arr[0], str_cmp);
	if (!str) {
		puts("bsearch: failed to find element.");
		rc = EXIT_FAILURE;
	}
	if (strcmp(str, "335") != 0) {
		puts("bsearch: found incorrect element.");
		rc = EXIT_FAILURE;
	}

	str = bsearch("", arr, ARRAY_SIZE(arr), sizeof arr[0], str_cmp);
	if (str) {
		puts("bsearch: found an element not in array.");
		rc = EXIT_FAILURE;
	}

	str = bsearch("999", arr, ARRAY_SIZE(arr), sizeof arr[0]-1, str_cmp);
	if (str) {
		puts("bsearch: found an element beyond end of array.");
		rc = EXIT_FAILURE;
	}

	str = bsearch("007", arr, ARRAY_SIZE(arr), sizeof arr[0], str_cmp);
	if (!str || strcmp(str, "007") != 0) {
		puts("bsearch: failed to find first element of array.");
		rc = EXIT_FAILURE;
	}

	str = bsearch("999", arr, ARRAY_SIZE(arr), sizeof arr[0], str_cmp);
	if (!str || strcmp(str, "999") != 0) {
		puts("bsearch: failed to find last element of array.");
		rc = EXIT_FAILURE;
	}

	str = bsearch("562", arr+3, ARRAY_SIZE(arr)-4, sizeof arr[0], str_cmp);
	if (!str || strcmp(str, "562") != 0) {
		puts("bsearch: failed to find element in shorter array.");
		rc = EXIT_FAILURE;
	}

	str = bsearch("395", arr+3, ARRAY_SIZE(arr)-7, sizeof arr[0], str_cmp);
	if (!str || strcmp(str, "395") != 0) {
		puts("bsearch: failed to find element in shorter array.");
		rc = EXIT_FAILURE;
	}

	return rc;
}
