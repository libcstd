/*
 * Copyright (C) 2010 Nick Bowler.
 *
 * License GPLv3+: GNU GPL version 3 or later.  See COPYING for terms.
 * See <http://www.gnu.org/licenses/gpl.html> if you did not receive a copy.
 * This is free software: you are free to change and redistribute it.
 * There is NO WARRANTY, to the extent permitted by law.
 */

#include <stdio.h>
#include <complex.h>
#include <assert.h>
#include "mathutil.h"

#undef creal
#undef cimag
#undef conj

int main(void)
{
	MATH_EXACT_ALL(creal,  1.0, 1.0 - 2.0 * I);
	MATH_EXACT_ALL(cimag, -2.0, 1.0 - 2.0 * I);

	MATH_EXACT_ALL(conj, 1.0 + 2.0 * I, 1.0 - 2.0 * I);

#ifdef __STDC_IEC_559__
	double complex x = 0 * I;

	if (!signbit(cimag(conj(x)))) {
		puts("conj: sign of zero imaginary part was unchanged.");
		return EXIT_FAILURE;
	}

	x = INFINITY * I;
	assert(isinf(cimag(x)));
	if (!signbit(cimag(conj(x)))) {
		puts("conj: sign of infinite imaginary part was unchanged.");
		return EXIT_FAILURE;
	}

	x = NAN * I;
	assert(isnan(cimag(x)));
	if (signbit(cimag(x)) == signbit(cimag(conj(x)))) {
		puts("conj: sign of NaN imaginary part was unchanged.");
		return EXIT_FAILURE;
	}
#endif

	return EXIT_SUCCESS;
}
