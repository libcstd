dnl Copyright (C) 2009 Nick Bowler
dnl Copying and distribution of this file, with or without modification,
dnl are permitted in any medium without royalty provided the copyright
dnl notice and this notice are preserved.  This file is offered as-is,
dnl without any warranty.

m4_pattern_forbid([^_?CSTD_[A-Z_]+$])

m4_define([_CSTD_WCHAR_T_UCHAR_PROG], [AC_LANG_PROGRAM([dnl
#include <stdint.h>
#include <limits.h>
#include <wchar.h>

#if WCHAR_MAX < UCHAR_MAX
#  error wchar_t cannot represent all unsigned char values.
#elif WEOF >= 0 && WEOF <= UCHAR_MAX
#  error WEOF value conflicts with unsigned char.
#endif
], [])])

AC_DEFUN([CSTD_WCHAR_T_UCHAR], [dnl
AC_CACHE_CHECK([if wchar_t can represent all unsigned char values],
	[cstd_cv_wchar_t_uchar],
	[AC_COMPILE_IFELSE([_CSTD_WCHAR_T_UCHAR_PROG],
		[cstd_cv_wchar_t_uchar=yes], [cstd_cv_wchar_t_uchar=no])])
])
